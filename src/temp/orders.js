export const testAdmin = {
    id: "abcd1234",
    name: "Adamastor",
    username: "admin",
    email: "adamastor@mail.com",
    password: "adamastor123"
}

export const testUser = {
    id: "user89723",
    firstName: "Dagomir",
    lastName: "Robervaldo",
    phone: "(12)93500-9816",
    birthday: "11/04/1998",
    email: "dagomir@mail.com",
    cpf: "343343543-19",
    password: "dagomir123"
}

export const ordersTemp = [
    {
        id: 0,
        userId: "user89723",
        date: "07-08-2022",
        status: "CLOSED",
        value: 40.99,
        description: "Joystick Wii white"
    },
    {
        id: 56,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    }, {
        id: 2310,
        userId: "user89723",
        date: "07-08-2022",
        status: "CLOSED",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    }, {
        id: 4540,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    }, {
        id: 7890,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 121,
        userId: "abcd1234",
        date: "07-08-2022",
        status: "CLOSED",
        value: 400,
        description: "Playstation 4 160GB"
    },
    {
        id: 2,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 3,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 4,
        userId: "abcd1234",
        date: "07-08-2022",
        status: "OPENED",
        value: 40.99,
        description: "Chuveiro elétrico a vapor"
    },
    {
        id: 5,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 6,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 7,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 8,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 9,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 10,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 11,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 12,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 13,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 14,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 15,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 16,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 17,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 18,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 19,
        userId: "user89723",
        date: "07-08-2022",
        status: "OPENED",
        value: 40.99,
        description: "Cabide comestível sabor caramelo"
    },
    {
        id: 20,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 21,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 22,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 23,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 24,
        userId: "user89723",
        date: "07-08-2022",
        status: "CLOSED",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 25,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 26,
        userId: "user89723",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 27,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 28,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 29,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 30,
        userId: "60d0fe4f5311236168a109e1",
        date: "07-08-2022",
        status: "OPENED",
        value: 40.99,
        description: "Alexa echo dot 4 with display"
    },
    {
        id: 31,
        userId: "user89723",
        date: "07-08-2022",
        status: "CLOSED",
        value: 40.99,
        description: "Ferro de passar usado"
    }
]