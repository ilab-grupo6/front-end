import { useState } from "react";
import { useAuth } from "../../contexts/AuthContext";
import { FormContainer } from "./styles";

function Form() {
  const [adminData, setAdminData] = useState({});
  const { validateLogin } = useAuth();

  return (
    <FormContainer onSubmit={(e) => e.preventDefault()}>
      <h1>Sign in</h1>

      <input
        type="text"
        placeholder="Email / Username"
        onChange={(e) =>
          setAdminData({ ...adminData, emailOrUsername: e.target.value })
        }
      />
      <input
        type="password"
        placeholder="Password"
        onChange={(e) =>
          setAdminData({ ...adminData, password: e.target.value })
        }
      />

      <button onClick={() => validateLogin(adminData)}>Sign in</button>
    </FormContainer>
  );
}

export default Form;
