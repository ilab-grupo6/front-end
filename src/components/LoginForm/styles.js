import styled from "styled-components";

export const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  width: 80%;
  justify-content: space-between;
  text-align: center;
  margin: 0 auto;
  font-family: Arial, Helvetica, sans-serif;
  align-items: center;

  h1 {
    color: #ea1d2c;
    margin-bottom: 8px;
  }

  input[placeholder="Name"],
  input[placeholder="Username"] {
    display: ${(props) => (props.isRegistered ? "none" : "block")};
    color: #a6a29f;
  }

  input {
    padding: 8px;
    font-size: 1rem;
    border: 1px solid #a6a29f;
    margin: 2px;
    outline: none;
    color: gray;
    height: 32px;
    width: 320px;
    border-radius: 4px;
    background-color: #fff;
    color: #a6a29f;

    ::placeholder {
      color: #a6a29f;
    }
  }

  button {
    border: none;
    height: 32px;
    width: 320px;
    border-radius: 4px;
    color: #fff;
    font-weight: bolder;
    margin-top: 4px;
  }

  p {
    margin-top: 8px;
  }

  .invalid {
    border: 1px solid red;
    color: #ea1d2c;
    ::placeholder {
      color: #ea1d2c;
    }
  }
`;
