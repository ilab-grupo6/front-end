import styled from 'styled-components';

export const Container = styled.div`
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  input {
    width: 230px;
    height: 32px;
    padding: 4px;
    border-radius: 5px;
    outline: none;
    border: 1px solid lightgray;
    color: gray;
    margin-right: 5px;
  }

  h1 {
      margin-top: 24px;
  }

  table {
      text-align: center;
      background-color: white;
      // min-width: 95%;
      min-width: 1200px;
      margin-top: 50px;
  }

  table, tr, td {
    border: 2px solid black;
    border-collapse: collapse;
  }

  td {
  padding: 4px;
    text-align: center;

    span {
      margin:  4px;
      padding: 4px;
      border-radius: 5px;

      :hover {
        cursor: pointer;
      }
    }
  }

  div {
    position: relative;
    width: 100%;

    h1 {
      color: black;
      font-weight: 500;
    }

    p {
      margin: 4px auto;
    }
  }

  button {
    padding: 8px;
    margin: 8px 4px;
    width: 120px;
    color: white;
    font-weight: bold;
    border-radius: 5px;
    border: none;
  }

  .insert-order{
    font-weight:bold;
  }

  .no-orders-cell {
    font-weight: 400;
    padding: 16px;
  }
`;
