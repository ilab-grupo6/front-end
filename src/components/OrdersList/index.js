// import React from 'react';

import { useState } from "react";
import { useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { api } from "../../api";
import { useAuth } from "../../contexts/AuthContext";
import RegisterOrderForm from "../RegisterOrderForm";
import { BsFillPencilFill, BsFillTrashFill } from "react-icons/bs";
import { Container } from "./styles";
import toast from "react-hot-toast";

function OrdersList({ user }) {
  const { token } = useAuth();

  const [userOrders, setUserOrders] = useState([]);
  const [userData, setUserData] = useState({})
  const [isHandlingOrder, setIsHandlingOrder] = useState(false);
  const userId = useLocation().state;
  useEffect(() => {
    api
      .get(`app1/orders/find-by-user-id/${user.id || userId}?sort=description,desc`, {
        headers: { Authorization: token },
      })
      .then((res) => {
        setUserOrders(res.data);
      });

      api.get(`/app2/users/${user.id || userId}`,  {
        headers: { Authorization: token },
      })
      .then(res => {
        setUserData(res.data)
      })
    // eslint-disable-next-line
  }, [isHandlingOrder]);

  const deleteOrder = (orderId) => {
    // setIsHandlingOrder(true);
    api
      .delete(`app1/orders/${orderId}`, { headers: { Authorization: token } })
      .then(() => {
        toast.success("order deleted!");
        window.location.reload();
        // setIsHandlingOrder(false)
      })

      .catch(() => toast.error("error deleting order!"));
  };

  const navigate = useNavigate();

  return (
    <Container>
      <div>
        <h1>{user.name}</h1>
          <p>Email: {userData.email}</p>
          <p>Phone: {userData.phone}</p>
          <p>CPF: {userData.cpf}</p>
      </div>
      <div>
        {!isHandlingOrder && (
          <div>
          <button
            className="insert-order"
            onClick={() => setIsHandlingOrder(true)}
          >
            New Order
          </button>
          <button onClick={() => window.location.reload()}>Refresh</button>
          </div>
        )}

        {isHandlingOrder && (
          <RegisterOrderForm
            setIsHandlingOrder={setIsHandlingOrder}
            userId={user.id || userId}
          />
        )}
      </div>
      <table>
        <thead>
          <tr>
            <th colSpan={6}>User Orders</th>
          </tr>
          {userOrders.length === 0? 
          <tr><th className="no-orders-cell">NO ORDERS FOR THIS USER :(</th></tr>
          :
          <tr>
            <th>id</th>
            <th>date</th>
            <th>description</th>
            <th>value</th>
            <th>status</th>
            <th>actions</th>
          </tr>
}
          {userOrders.sort((a, b) => {
            return new Date(b.orderDate) - new Date(a.orderDate)
          }).map((order) => {
            return (
              <tr key={order.id}>
                <td>{order.id}</td>
                <td>{new Date(order.orderDate).toLocaleString("pt")}</td>
                <td>{order.description}</td>
                <td>
                  {new Intl.NumberFormat("pt-br", {
                    style: "currency",
                    currency: "BRL",
                    minimumFractionDigits: 2,
                  }).format(order.value / 100)}
                </td>
                <td>{order.status}</td>
                <td>
                  <span
                    onClick={() => navigate("/edit-order", { state: order })}
                  >
                    <BsFillPencilFill fill="black" />
                  </span>
                  <span onClick={() => deleteOrder(order.id)}>
                    <BsFillTrashFill fill="black" />
                  </span>
                </td>
              </tr>
            );
          })}
        </thead>
      </table>
      <button onClick={() => navigate("users")}>back</button>
    </Container>
  );
}

export default OrdersList;
