import { useState } from "react";
import CurrencyInput from "react-currency-masked-input";
import toast from "react-hot-toast";
import { api } from "../../api";
import { useAuth } from "../../contexts/AuthContext";
import { Container } from "./styles";

function RegisterOrderForm({ setIsHandlingOrder, userId }) {
  const [orderData, setOrderData] = useState({
    userId,
  });

  const { token } = useAuth();

  const onSubmit = (e) => {
    e.preventDefault();

    api
      .post("/app1//orders", orderData, {
        headers: {
          Authorization: token,
        },
      })
      .then(() => {
        toast.success("order created!");
        setIsHandlingOrder(false);
      })
      .catch(() => toast.error("error creating order"));
  };


  return (
    <Container>
      <h1>INSERT ORDER</h1>
      <form onSubmit={(e) => onSubmit(e)}>
        <input
          className="input-register"
          type="text"
          placeholder="DESCRIPTION"
          onChange={(e) =>
            setOrderData({ ...orderData, description: e.target.value })
          }
        />

        <CurrencyInput
          name="myInput"
          required
          className="input-register"
          type="number"
          step="0.01"
          placeholder="VALUE"
          onChange={(e) =>
            setOrderData({ ...orderData, value: e.target.value * 1000 })
          }
        />
        <div className="buttons-save-and-cancel">
          <button className="save">save</button>
          <button onClick={() => setIsHandlingOrder(false)}>cancel</button>
        </div>
      </form>
    </Container>
  );
}

export default RegisterOrderForm;
