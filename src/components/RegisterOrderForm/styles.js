import styled from 'styled-components';

export const Container = styled.div`

  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  h1{
    margin-bottom: 20px;
  }
  
  form {
    display: flex;
    flex-direction: column;
    gap:10px;
  }

  .input-register {
    width: 300px;
    height: 32px;
    padding: 4px;
    border-radius: 5px;
    outline: none;
    border: 1px solid lightgray;
    color: gray;
  }

  .save{
    margin-right: 20px;
  }

`;
