import moment from "moment";
import { useAuth } from "../../contexts/AuthContext";
import { Container } from "./styles";

function Header() {
  const { admName, isAuthenticated, logout } = useAuth();

  const today = moment().format("LL");

  return (
    isAuthenticated && (
      <>
        <Container>
          <div>
            <p>
              Hello, <span>{admName}</span>
            </p>
            <p>{today}</p>
          </div>
          <h1>Admin Panel</h1>
          <button onClick={logout}>logout</button>
        </Container>
      </>
    )
  );
}

export default Header;
