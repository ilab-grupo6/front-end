import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  padding: 16px;
  background-color: #ffffff;
  display: flex;
  justify-content: space-between;
  margin-bottom:70px;

  button {
    border: none;
    padding: 8px;
    width: 80px;
    height: 32px;
    color: white;
    font-weight: 500;
    border-radius: 5px;
  }

  div > p > span {
    text-transform: capitalize;
  }

  border-bottom: 1px solid black;
`;
