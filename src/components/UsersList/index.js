import React, { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { api } from "../../api";
import { useAuth } from "../../contexts/AuthContext";
import { Container } from "./styles";
import { BsFillPencilFill, BsFillTrashFill } from "react-icons/bs";
import { FaShoppingCart } from "react-icons/fa";

function UsersList({ page, isLoading, setIsLoading, setLastPage, lastPage }) {
  const [users, setUsers] = useState([]);
  const [searchString, setSearchString] = useState("");
  const [isSearchingUser, setIsSearchingUser] = useState(false);
  const [foundUsers, setFoundUsers] = useState([]);
  const { token, logout } = useAuth();
  const navigate = useNavigate();
      // eslint-disable-next-line
  const [isHandlingUser, setIsHandlingUser] = useState(false);

  useEffect(() => {
    api
      .get(`/app2/users?page=${page}&size=10&sort=name,asc`, {
        headers: { Authorization: token },
      })
      .then((res) => {
        setUsers(res.data.content);
        setLastPage(res.data.totalPages - 1);
      })
      .then(setIsLoading(false))
      .catch(() => {
        toast.error("expired session!");
        logout();
      });

    // eslint-disable-next-line
  }, [page, isHandlingUser]);

  const openOrders = (user) => {
    navigate("/user-orders", { state: user });
  };

  const editUser = (user) => {
    navigate("/edit-user", { state: user });
  };

  const findUser = (e) => {
    e.preventDefault();
    api
      .get(`/app2/users/find?name=${searchString}`, {
        headers: { Authorization: token },
      })
      .then((res) => {
        setFoundUsers(res.data);
        res.data && setIsSearchingUser(true);
      });
  };

  const deleteUser = (userId) => {
    api
      .delete(`/app2/users/delete/${userId}`, {
        headers: {
          Authorization: token,
        },
      })
      .then(() => toast.success("user deleted!"))
      .then(() => window.location.reload())
      .catch(() => toast.error("error deleting user!"));
  };

  return (
    <Container>
      {isLoading ? (
        <h1>Loading...</h1>
      ) : (
        <>
          <div>
            <form onSubmit={(e) => findUser(e)}>
              <input
                type="text"
                placeholder="FIND USER"
                onChange={(e) => setSearchString(e.target.value)}
              />
              <button className="find-user-btn" onClick={findUser}>
                FIND
              </button>
            </form>
            <button
              className="insert-user"
              onClick={() => navigate("/register-user")}
            >
              NEW USER
            </button>
          </div>

          <table>
            <thead>
              <tr>
                <th className="user-id">id</th>
                <th>name</th>
                <th>email</th>
                <th>cpf</th>
                <th>birthdate</th>
                <th>phone</th>
                <th>actions</th>
              </tr>
            </thead>
            {foundUsers &&
              foundUsers.map((user) => {
                return (
                  <tbody>
                    <tr>
                      <td>{user.id}</td>
                      <td>{user.name}</td>
                      <td className="email-cell">{user.email}</td>
                      <td>{user.cpf}</td>
                      <td>{user.birth}</td>
                      <td>{user.phone}</td>
                      <td className="user-actions">
                        <span>
                          <BsFillPencilFill
                            onClick={() => editUser(user)}
                            fill="black"
                          />
                        </span>
                        <span>
                          <BsFillTrashFill
                            onClick={() => deleteUser(user.id)}
                            fill="black"
                          />
                        </span>
                        <span onClick={() => openOrders(user)}>
                          <FaShoppingCart fill="black" />
                        </span>
                      </td>
                    </tr>
                  </tbody>
                );
              })}
            {!isSearchingUser && (
              <tbody>
                {users &&
                  users.map((user) => {
                    return (
                      <tr key={user.id}>
                        <td className="user-id">{user.id}</td>
                        <td
                          className="user-name"
                          onClick={() => openOrders(user)}
                        >
                          {user.name}
                        </td>
                        <td className="email-cell">{user.email}</td>
                        <td>{user.cpf}</td>
                        <td>{user.birth}</td>
                        <td>{user.phone}</td>

                        <td className="user-actions">
                          <span onClick={() => editUser(user)}>
                            <BsFillPencilFill fill="black" />
                          </span>
                          <span onClick={() => deleteUser(user.id)}>
                            <BsFillTrashFill fill="black" />
                          </span>
                          <span onClick={() => openOrders(user)}>
                            <FaShoppingCart fill="black" />
                          </span>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            )}
          </table>

          {!isSearchingUser && `${page + 1} / ${lastPage + 1}`}
          {isSearchingUser && (
            <button
              className="back-btn"
              onClick={() => {
                window.location.reload();
              }}
            >
              back
            </button>
          )}
        </>
      )}
    </Container>
  );
}

export default UsersList;
