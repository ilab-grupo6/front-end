import styled from "styled-components";

export const Container = styled.div`
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 0 auto;

  input {
    width: 230px;
    height: 32px;
    padding: 4px;
    border-radius: 5px;
    outline: none;
    border: 1px solid lightgray;
    color: gray;
  }

  div {
    position: relative;
    width: 100%;
    margin: 0 auto;
  }

  .find-user-btn {
    height: 32px;
    padding: 8px;
    width: 48px;
  }

  .insert-user {
    position: absolute;
    right: 0;
    height: 34px;
    padding: 8px;
    font-weight: bold;
    font-size: 12px;
  }

  .user-actions {
    span {
      padding: 0 4px;
      // background-color: lightgray;
      color: black;
      margin: 2px;
      border-radius: 4px;
    }
  }

  table {
    // width: 95%;
    min-width: 1200px;
    background-color: white;
    color: #212121;
    text-align: center;
    margin-top: 50px;
    margin: 50px auto;
  }

  table,
  th,
  td {
    border: 2px solid black;
    border-collapse: collapse;
  }

  th {
    text-transform: uppercase;
  }

  td {
    padding: 4px 8px;
    text-align: center;
    width: 180px;
    max-width: 180px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

    span {
      color: blue;
      cursor: pointer;

      :hover {
        text-decoration: underline;
      }
    }
  }

  .email-cell {
    width: 250px;
    max-width: 240px;
  }

  div {
    margin-bottom: 12px;
  }

  .user-id {
    max-width: 120px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

    :hover {
      ::before {
        content: ${(props) => props.userId};
        width: 200px;
        position: absolute;
        left: 10%;
        background-color: lightblue;
        border-radius: 5px;
      }
    }
  }

  .user-name {
    cursor: pointer;

    :hover {
      text-decoration: underline;
    }
  }

  div button {
    color: white;
    width: auto;
    font-size: small;
    height: auto;
  }

  .back-btn {
    width: 80px;
    font-size: 1rem;
    color: gray;
    margin: 4px 0;
    color: #fff;
  }

  h1 {
    transform: translateY(500%);
  }
`;
