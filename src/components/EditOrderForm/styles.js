import styled from "styled-components";

export const Container = styled.div`
  h1 {
    color: black;
  }

  label {
    text-transform: uppercase;
  }

  textarea {
    padding: 2px 4px;
  }
  form {
    display: flex;
    flex-direction: column;
    height: 85vh;
    justify-content: flex-start;
    text-align: center;
    margin: 0 auto;
    align-items: center;
    width: 720px;
    gap: 15px;

    div {
      display: flex;
      justify-content: space-between;
      width: 460px;
      gap: 20px;
      text-align: center;

      input {
        height: fit-content;
        width: 320px;
      }

      textarea {
        width: 320px;
        height: 160px;
      }
    }

    input {
      padding: 8px;
      font-size: 1rem;
      border: 1px solid #a6a29f;
      margin: 2px;
      outline: none;
      color: black;
      height: 32px;
      width: 320px;
      border-radius: 4px;
      background-color: #fff;
    }

    button {
      border: none;
      height: 32px;
      width: 320px;
      border-radius: 4px;
      color: #fff;
      font-weight: bolder;
      margin-top: 4px;
    }

    .blocked-input {
      cursor: not-allowed;
      color: #a6a29f;
    }
  }
`;
