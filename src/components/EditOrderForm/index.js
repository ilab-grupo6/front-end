import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import CurrencyInput from "react-currency-masked-input";
import toast from "react-hot-toast";
import { useLocation, useNavigate } from "react-router-dom";
import { api } from "../../api";
import { useAuth } from "../../contexts/AuthContext";

import { Container } from "./styles";

function EditOrderForm() {
  const [orderData, setOrderData] = useState(useLocation.state);
  const { token } = useAuth();
  const navigate = useNavigate();
  const order = useLocation().state;

  useEffect(() => {
    api
      .get(`/app1/orders/${order.id}`, {
        headers: {
          Authorization: token,
        },
      })
      .then((res) => {
        setOrderData(res.data);
      });
  }, [order, token]);

  const submit = (e) => {
    e.preventDefault();
    const formatedData = { ...orderData, userId: orderData.userId };
    api
      .put(`app1/orders/${orderData.id}`, formatedData, {
        headers: {
          Authorization: token,
        },
      })
      .then(() => {
        toast.success("order updated!");
        navigate("/user-orders", { state: orderData.userId });
      })
      .catch(() => toast.error("failed to update order!"));
  };

  return (
    <Container>
      {orderData && (
        <form onSubmit={(e) => submit(e)}>
          <h1>EDIT ORDER</h1>

          <div>
            <label>Order id</label>
            <input
              type="text"
              defaultValue={orderData.id}
              disabled
              className="blocked-input"
            />
          </div>
          <div>
            <label>User id</label>
            <input
              type="text"
              defaultValue={orderData.userId}
              disabled
              className="blocked-input"
            />
          </div>

          <div>
            <label htmlFor="">Description</label>
            <textarea
              type="textarea"
              defaultValue={orderData.description}
              onChange={(e) =>
                setOrderData({ ...orderData, description: e.target.value })
              }
            />
          </div>
          <div>
            <label htmlFor="">Value</label>
            <CurrencyInput
              defaultValue={orderData.value / 100}
              onChange={(e) =>
                setOrderData({
                  ...orderData,
                  value: Number(e.target.value * 1000),
                })
              }
            />
          </div>

          <div>
            <label htmlFor="">Status</label>
            <input
              type="text"
              defaultValue={orderData.status}
              disabled
              className="blocked-input"
            />
          </div>
          <div>
            <button type="submit">Save</button>
            <button type="button" onClick={() => navigate(-1)}>
              Cancel
            </button>
          </div>
        </form>
      )}
    </Container>
  );
}

export default EditOrderForm;
