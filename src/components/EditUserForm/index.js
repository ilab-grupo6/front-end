import React from "react";
import { useState } from "react";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { api } from "../../api";
import { useAuth } from "../../contexts/AuthContext";
import { Container } from "./styles";
import InputMask from "react-input-mask";

function EditUserForm({ user }) {
  const [userData, setUserData] = useState(user);
  const navigate = useNavigate();
  
  const {token} = useAuth();

  const submit = (e) => {
    delete userData["cpf"]
    e.preventDefault();
    api.put(`/app2/users/update/${user.id}`, userData, {headers: {
        Authorization: token
    }})
    .then(() => {
      toast.success("user updated!")
      navigate("/users")
    })
      
    .catch(() => toast.error("error updating user!"))
  };

  return (
    <Container>
      <form onSubmit={(e) => submit(e)}>
        <h1>EDIT USER</h1>
        <input
          type="text"
          defaultValue={userData.name}
          onChange={(e) => setUserData({ ...userData, name: e.target.value })}
        />
        <input
          type="text"
          placeholder={user.cpf}
          disabled
          className="blocked-input"
        />
        <InputMask
        mask="(99)99999-9999"
          type="text"
          defaultValue={userData.phone}
          onChange={(e) => setUserData({ ...userData, phone: e.target.value })}
        />
        <input
          type="text"
          defaultValue={user.birth}
          disabled
          className="blocked-input"
        />
        <input
          type="text"
          defaultValue={userData.email}
          onChange={(e) => setUserData({ ...userData, email: e.target.value })}
        />
        <button>Save</button>
        <button onClick={() => navigate("/users")}>Cancel</button>
      </form>
    </Container>
  );
}

export default EditUserForm;
