import styled from "styled-components";

export const Container = styled.div`
  form {
    display: flex;
    flex-direction: column;
    height: 85vh;
    justify-content: flex-start;
    text-align: center;
    margin: 0 auto;
    align-items: center;

    input {
      padding: 8px;
      font-size: 1rem;
      border: 1px solid #a6a29f;
      margin: 2px;
      outline: none;
      color: black;
      height: 32px;
      width: 320px;
      border-radius: 4px;
      background-color: #fff;
    }

    button {
      border: none;
      height: 32px;
      width: 320px;
      border-radius: 4px;
      color: #fff;
      margin-top: 4px;
    }

    .blocked-input {
      cursor: not-allowed;
      color: #a6a29f;
    }
  }
`;
