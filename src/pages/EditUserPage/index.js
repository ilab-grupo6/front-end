import React from 'react';
import { useLocation } from 'react-router-dom';
import EditUserForm from '../../components/EditUserForm';

import { Container } from './styles';

function EditUserPage() {
  
    const user = useLocation().state
  
    return (
    <Container>
        <EditUserForm user={user} />
    </Container>
  );
}

export default EditUserPage;