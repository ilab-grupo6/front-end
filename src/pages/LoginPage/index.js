import LoginForm from '../../components/LoginForm';
import { Container } from './styles';

function LoginPage() {
  return (
  <Container>
    <LoginForm />
  </Container>
  )
}

export default LoginPage;