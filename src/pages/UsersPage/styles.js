import styled from 'styled-components';

export const Container = styled.div`
  /* width: 100%; */
  max-width: 1720px;
  margin: 0 auto;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  
  h1 {
    color: black;
  } 

  button {
    margin: 0 8px;
    border: none;
    font-size: 2rem;
    border-radius: 5px;
    padding: 4px 8px;
    color: white;
  }

  .next-btn, .previous-btn {
    margin: auto auto;
    position: relative;
    transform: translateY(50%);
  }
`;
