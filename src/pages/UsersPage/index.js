import React, { useState } from "react";
import UsersList from "../../components/UsersList";

import { Container } from "./styles";

function UsersPage() {
  const [page, setPage] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [lastPage, setLastPage] = useState(false);

   // eslint-disable-next-line
  const [pageButtonsVisibility, setPageButtonsVisibility] = useState({
    previousBtn: false,
    nextBtn: true 
  })

  const changePage = (operation) => {
      if (operation === "next" && (page < lastPage)) {
        setPage(page + 1);
        setIsLoading(true);
        
      } if (operation === "previous" && page > 0) {
        setPage(page - 1);
        setIsLoading(true);
    }
  };

  return (
    <>
      <Container>
        {!isLoading &&
        <button className="previous-btn" onClick={() => changePage("previous")} pageButtonsVisibility={pageButtonsVisibility}>
          &#x2190;
        </button>
}
        <UsersList
          page={page}
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          setLastPage={setLastPage}
          lastPage={lastPage}
        />
   {!isLoading &&
        <button className="next-btn" onClick={() => changePage("next")} pageButtonsVisibility={pageButtonsVisibility} >
          &#x2192;
        </button>
}
      </Container>
    </>
  );
}

export default UsersPage;
