import React from "react";
import { useLocation } from "react-router-dom";
import EditOrderForm from "../../components/EditOrderForm";

import { Container } from "./styles";

function EditOrderPage() {
  const order = useLocation().state;
  return (
    <Container>
      <EditOrderForm order={order} />
    </Container>
  );
}

export default EditOrderPage;
