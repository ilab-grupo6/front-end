import React, { useState } from "react";
import { Container, FormContainer } from "./styles";
import { useAuth } from "../../contexts/AuthContext";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate } from "react-router-dom";
import InputMask from "react-input-mask";

function RegisterUserPage() {
  const navigate = useNavigate();

  // eslint-disable-next-line
  const [isRegistered, setIsRegistered] = useState(true);
  const [userData, setUserData] = useState({});
  const { registerUser } = useAuth();

  const submit = () => {};

  const formSchema = yup.object().shape({
    name: yup.string().required(isRegistered ? false : true),
    cpf: yup.string().required(isRegistered ? false : true),
    phone: yup.string().required(isRegistered ? false : true),
    birth: yup.string().required(isRegistered ? false : true),
    email: yup
      .string()
      .required(isRegistered ? false : true)
      .email("Email inválido"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  return (
    <Container>
      {/* tela de registrar */}
      <FormContainer
        isRegistered={isRegistered}
        onSubmit={handleSubmit(submit)}
      >
        <h1>NEW USER</h1>
        <input
          type="text"
          placeholder="Name"
          {...register("name")}
          onChange={(e) => setUserData({ ...userData, name: e.target.value })}
          className={errors.name && "invalid"}
        />
        <InputMask
          type="text"
          mask="999.999.999-99"
          placeholder="CPF"
          {...register("cpf")}
          onChange={(e) => setUserData({ ...userData, cpf: e.target.value })}
          className={errors.cpf && "invalid"}
        />
        <InputMask
          type="text"
          mask="(99)99999-9999"
          placeholder="Phone"
          {...register("phone")}
          onChange={(e) => setUserData({ ...userData, phone: e.target.value })}
          className={errors.phone && "invalid"}
        />
        <InputMask
          type="text"
          mask="99/99/9999"
          placeholder="Birth"
          {...register("birth")}
          onChange={(e) => setUserData({ ...userData, birth: e.target.value })}
          className={errors.birth && "invalid"}
        />
        <input
          type="text"
          placeholder="Email"
          {...register("email")}
          onChange={(e) => setUserData({ ...userData, email: e.target.value })}
          className={errors.email && "invalid"}
        />
        <div>
          <button onClick={() => registerUser(userData)}>Register</button>
          <button onClick={() => navigate("/users")}>Cancel</button>
        </div>
      </FormContainer>
    </Container>
  );
}

export default RegisterUserPage;
