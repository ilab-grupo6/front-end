import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 85vh;
    justify-content: center;
    text-align: center;
    margin: 0 auto;
    align-items: center;

`;

export const FormContainer = styled.form`
  
    width: 80%;
    justify-content: flex-start;
    font-family: Arial, Helvetica, sans-serif;
    display: flex;
    flex-direction: column;
    height: 100vh;
    text-align: center;
    margin: 0 auto;
    align-items: center;
  

  h1 {
    color: black;
    margin-bottom: 20px
  }

  input {
    padding: 8px;
    font-size: 1rem;
    border: 1px solid #A6A29F ;
    margin: 2px;
    outline: none;
    color: gray;
    height: 32px;
    width: 320px;
    border-radius: 4px;
    background-color: #fff;
    color: #A6A29F;

    ::placeholder {
        color: #A6A29F;
    }
  
  }

  button {
    border: none;
    height: 32px;
    width: 320px;
    border-radius: 4px;
    color: #fff;
    font-weight: bolder;
    margin-top: 4px;
  }

  .invalid {
        border: 1px solid red;
        color: #ea1d2c;
        ::placeholder {
            color: #ea1d2c;
        }
    }

    > div > button {
      width: 150px;
      margin: 4px 10px;
    }
`;

