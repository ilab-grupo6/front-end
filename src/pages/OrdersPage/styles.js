import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 93vh;
  display: flex;
  align-items: flex-start;
  justify-content: center;
`;
