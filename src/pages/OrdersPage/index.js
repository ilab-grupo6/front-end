import React from "react";
import { useLocation } from "react-router-dom";
import OrdersList from "../../components/OrdersList";

import { Container } from "./styles";

function OrdersPage() {

  const user = useLocation().state;
  
  return (
    <Container>
      <OrdersList user={user} />
    </Container>
  );
}

export default OrdersPage;
