import { createGlobalStyle } from "styled-components";
import "../assets/index.css"

export const GlobalStyles = createGlobalStyle`
    
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: "Sul Sans";
    }
    
    body {
        font-family: "Helvetica";
        background-color: #ededed; ;
    }

    button {
    cursor: pointer;
    background-color: #ea1d2c;
    text-transform: uppercase;

    :hover {
        filter: brightness(90%);
        transition: .2s ease;
    }
}

h1 {
    color: #ea1d2c;
}

th {
    text-transform: uppercase;
}

`;