import {Routes as Switch, Route, Navigate} from "react-router-dom";
import LoginPage from "../pages/LoginPage"
import UsersPage from "../pages/UsersPage"
import OrdersPage from "../pages/OrdersPage"
import RegisterUserPage from "../pages/RegisterUserPage"
import EditUserPage from "../pages/EditUserPage"
import { useAuth } from "../contexts/AuthContext";
import EditOrderPage from "../pages/EditOrderPage";

const Routes = () => {

    const {isAuthenticated} = useAuth();

    return (
        <>
          {!isAuthenticated ? (
            <Switch>
              <Route path="/login" element={<LoginPage />} />
              <Route path="*" element={<LoginPage />} />
            </Switch>
          ) : (
            <Switch>
              <Route path="/users" element={<UsersPage />} />
              <Route path="/user-orders" element={<OrdersPage />} />
              <Route path="/register-user" element={<RegisterUserPage/>} />
              <Route path="/edit-user" element={<EditUserPage/>} />
              <Route path="/edit-order" element={<EditOrderPage/>} />
              <Route path="*" element={<Navigate to="/users" replace />} />
            </Switch>
          )}
        </>
      );
    };
    
    export default Routes;