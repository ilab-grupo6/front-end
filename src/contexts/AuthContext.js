import { createContext, useContext, useEffect, useState } from "react";
import toast from "react-hot-toast";
import jwtDecode from "jwt-decode";
import { api } from "../api"
import { useNavigate } from "react-router-dom";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [token, setToken] = useState("");
  const [isAuthenticated, setIsAutenticated] = useState(false);
  //eslint-disable-next-line
  const [admName, setAdmName] = useState("");

  const navigate = useNavigate();

  useEffect(() => {

    const getToken = localStorage.getItem("@iLab/token") || ""

    if (getToken) {
      setIsAutenticated(true);
      setToken(getToken);
      const decoded = jwtDecode(getToken)
      setAdmName(decoded.sub)
    }
    //eslint-disable-next-line
  }, [token]);


  const validateLogin = (adminData) => {
    const loginData = {
      email: adminData.emailOrUsername,
      username: adminData.emailOrUsername,
      password: adminData.password,
    };

    api
      .post("/app/login", loginData)
      .then((res) => {
        toast.success("login succesfull!");
        localStorage.setItem("@iLab/token", res.data.token);
        setToken(res.data.token)
        setIsAutenticated(true)
      })
      .catch(() => toast.error("invalid login data!"));
  };

  const registerUser = (userData) => {
    const formatedUserData = {
      name: userData.name,
      cpf: userData.cpf,
      phone: userData.phone,
      birth: userData.birth,
      email: userData.email,
    };

    api.post("/app2/users/register", formatedUserData, {headers: {
      Authorization: token
    }})
    .then(
      () => toast.success("user registered!"))
      .then(() => navigate("/users"))
      .catch((response) => {
        const error = response.response.data.message;
        error.includes("already")? toast.error("cpf or email already registered") : error.includes("valid")? toast.error("cpf must be valid!") : toast.error("error registering user!")
      });

  };

  const logout = () => {
    localStorage.removeItem("@iLab/token");
    setIsAutenticated(false);
    window.location.replace("/login");
  };

  return (
    <AuthContext.Provider
      value={{
        token,
        admName,
        registerUser,
        validateLogin,
        isAuthenticated,
        logout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
