import { useLocation } from "react-router-dom";
import Header from "./components/Header";
import Routes from "./routes/routes";

function App() {
  const { pathname } = useLocation();
  
  return (
    <>
      {pathname !== "/login" && pathname !== "/" && <Header />}
      <Routes />
    </>
  );
}

export default App;
