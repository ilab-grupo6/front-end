#FROM node:lts-alpine
#COPY . .
#RUN npm install
#RUN npm install react-scripts -g
# RUN npm run build

FROM nginx:latest
COPY . /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

# EXPOSE 3000
# COPY . .
#CMD ["npm", "start"]